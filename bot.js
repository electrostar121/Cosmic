/*
This is the Cosmic Dust Discord Bot
*/

//All the imported Modules
const Discord = require("discord.js");
const fs = require("fs");
const path = require("path");
const botInfo = require("./package.json");
const server = require("./guild");
const mk8 = require("./mariokart");

//The config file, its where the bot token is
let config = require("./config.json");

//Creates the bot client and supplies the bot token
const client = new Discord.Client();
client.login(config.botToken);

//Sets the bot activity
client.on('ready', () => {
	client.user.setActivity("the Stars", {"type": "WATCHING"});
});

var prefix = ']';//prefix of the bot

//Meat and potatoes of the bot
client.on("message", function(message){

	if(message.author.bot) return;//Checks to see if the message was from a bot or user, if from bot it just doesn't do anything

	if(!message.content.startsWith(prefix)) return;//Checks to see if the first character is the prefix, if not it doesn't do anything

	//Formats the contents of the message once its passed all it's checks from earlier
	const commandBody = message.content.slice(prefix.length);
	const args = commandBody.split(' ');
	const command = args.shift().toLowerCase();

	if(command === "info"){//Info command: provides info about the bot
	
		message.channel.send(`This discord bot contains various modules that does various things. It is to be a multi purpose bot.`);
		message.channel.send(`Currently this bot is on v${botInfo.version} of it's code`);
		message.channel.send(`The bot's head dev is ${botInfo.author}`);
	
	}
	
	if(command === "ping"){//Ping command: gives ping of the bot
	
		const timeTaken = Date.now() - message.createdTimestamp;
		message.channel.send(`Pong: ${timeTaken}ms`);
	
	}

	if(command === "date"){//Date command: provides the date

		message.channel.send(new Date().toString());
	
	}

	if(command === "update" && message.member.permissions.has('ADMINISTRATOR')){//Update command: to update the perms of the guild for each of the commands
	
		if(!(args.length < 2 || args.length > 2)){
			
			if(args[1] === 'true' || args[1] === 'false'){	
			
				if(server.updatePerm(message.guild.id.toString(), args[0], args[1])){
		
					message.channel.send(`The guild perms for ${args[0]} has been updated`);
		
				}else{
		
					message.channel.send(`The command ${args[0]} does not exist for this bot`);
		
				}

			}else{
		
				message.channel.send('Please use true or false for updating the perm');
		
			}

		}else{
		
			message.channel.send('The structure of the command follows:');
			message.channel.send('```]update {command name} {true or false}```');
		
		}
	
	}

	if(command === "perms" && message.member.permissions.has('ADMINISTRATOR')){//Perms command: prints out all the avaible commands for the server
	
		perms = server.printPerms(message.guild.id.toString());
		var i = 0;

		message.channel.send(`Here are the list of commands enabled/disabled for ${message.guild.name}:`);

		while(i < perms.length){
		
			message.channel.send(`${perms[i]}: ${perms[i+1]}`);

			i+=2;
		
		}
	
	}

	//The Mariokart command
	if(command === "mk" && ('true' === server.canDo(message.guild.id.toString(), "mk"))){
		
		//The pick subcommand: Picks random tracks from a mk game. If the user doesn't input a mk game it will default to mk8. If user doesn't input the amount of tracks it will default to 3.
		if(args[0] === "pick"){
		
			if(args.length === 1){//If no additional arguments provided
			
				const pick = mk8.randomTrack("mk8", 3);

				message.channel.send(`Here are 3 random tracks from mk8`);
				message.channel.send(`${pick[0]}`);
				message.channel.send(`${pick[1]}`);
				message.channel.send(`${pick[2]}`);

			}else if(args.length === 2){//If one argument is provided. Must be an integer
				
				if(!isNaN(args[1]) && Number.isInteger(parseFloat(args[1]))){
				
					const pick = mk8.randomTrack("mk8", args[1]);

					if(pick[pick.length-1] === true){

						var i = 0;

						message.channel.send(`Here are ${args[1]} random tracks from mk8`);

						while(i < pick.length-2){
				
							message.channel.send(`${pick[i]}`);
							i++;
				
						}

					}else{
					
						message.channel.send(`The max number of tracks you can request from mk8 is ${pick[pick.length-2]}`);

					}

				}else{//Error message if argument isn't an integer
				
					message.channel.send(`Error! ${args[1]} is not valid. Please input a valid number.`);
				
				}

			}else if(args.length === 3){//If two arguments provided. First argument must be an integer, second argument must be a mk game
			
				var mkList = new Array();
				
				fs.readdirSync('./mkTracks/').forEach(file => {//Builds the list of valid mk games

					mkList.push(file);

				});
				
				if(!isNaN(args[1]) && Number.isInteger(parseFloat(args[1]))){
				
					if(mkList.includes(args[2])){
					
						const pick = mk8.randomTrack(args[2], args[1]);

						if(pick[pick.length-1] === true){

							var i = 0;

							message.channel.send(`Here are ${args[1]} random tracks from mk8`);

							while(i < pick.length-2){
				
								message.channel.send(`${pick[i]}`);
								i++;
				
							}

						}else{
					
							message.channel.send(`The max number of tracks you can request from ${args[1]} is ${pick[pick.length-2]}`);

						}

					}else{//Error message if not a valid mk game
					
						var i = 0

						message.channel.send(`Error! ${args[2]} is an invalid mk game. Valid games are:`);

						while(i < mkList.length){
						
							message.channel.send(mkList[i]);
							i++;
						
						}
					
					}
				
				}else{//Error message if argument isn't an integer
				
					message.channel.send(`Error! ${args[1]} is not valid. Please input a valid number.`);
				
				}
			
			}
		
		}
		
		//Checks to see if there are no arguments
		if(args.length === 0){
		
			message.channel.send(`Please supply additional arguments!!`);
		
		}

	}

});
