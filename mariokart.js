/*
The is the Mariokart Module for the CD Bot
*/

const fs = require('fs');

//Random Track Function: Returns 3 random tracks, makes sure that they are not the same
function randomTrack(mkgame, amount){
	
	var tracks = new Array();
	var count = Number(amount);
	var flag = true;
	
	const data = fs.readFileSync(`./mkTracks/${mkgame}`, 'UTF-8');
	var lines = data.split(/\r?\n/);

	lines.forEach((line) => {//Builds the track list from provided mk game
	
		tracks.push(line);
	
	});

	tracks.pop();//Gets rid of the null element at the end of the track list

	var maxTracks = tracks.length;

	if(maxTracks < count){
	
		count = maxTracks;
		flag = false;
	
	}

	var i = tracks.length - count - 1;//Figures out how many tracks to remove from the track list

	while(i >= 0){//Picks n number of tracks
	
		tracks.splice(Math.floor(Math.random()*tracks.length), 1);
		i--;
	
	}

	i = tracks.length - 1;

	while(i > 0 && flag === true){//Shuffles the track list using the Durstenfeld shuffle
	
		const j = Math.floor(Math.random() * (i + 1));

		[tracks[i], tracks[j]] = [tracks[j], tracks[i]];

		i--;

	}

	tracks.push(maxTracks);
	tracks.push(flag);

	return tracks;//returns the selected tracks

}

module.exports = { randomTrack };//Module exports
