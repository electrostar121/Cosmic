//modules required for the guild module
const fs = require("fs");
const path = require("path");

//the canDo function: sees if the guild can access a certain command
function canDo(guildId, command){

	const guild = require(`./guildperms/${guildId}.json`);
	const guildS = JSON.stringify(guild);
	const guildP = JSON.parse(guildS);

	console.log(guildP[`${command}`]);

	return guildP[`${command}`];

}

function printPerms(guildId){//returns all the commands and if they are enabled or not

	let guild = require(`./guildperms/${guildId}.json`);
	const data = fs.readFileSync(`./commandList`, 'UTF-8');
	var lines = data.split(/\r?\n/);
	var commands = new Array();
	var perms = new Array();

	lines.forEach((line) => {
	
		commands.push(line);
	
	});

	commands.pop();

	var i = 0;

	while(i < commands.length){
	
		perms.push(commands[i]);
		perms.push(guild[`${commands[i]}`]);
		i++;
	
	}

	return perms;

}

function updatePerm(guildId, command, allow){//updates the command to be enabled or disabled

	var err = false;
	
	const guildFile = `./guildperms/${guildId}.json`;
	const guild = require(guildFile);
	
	const data = fs.readFileSync(`./commandList`, 'UTF-8');
	var lines = data.split(/\r?\n/);
	var commands = new Array();

	lines.forEach((line) => {
	
		commands.push(line);
	
	});

	commands.pop();

	if(commands.includes(command)){

		guild[`${command}`] = allow;

		fs.writeFile(guildFile, JSON.stringify(guild), function writeJSON(err){

			if(err) return console.log(err);
			console.log(JSON.stringify(guild));
			console.log('writing to ' + guildFile);
	
		});

		err = true;

	}

	return err;

}

module.exports = { canDo, printPerms, updatePerm };//exports the module
